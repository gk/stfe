module github.com/system-transparency/stfe

go 1.14

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/mock v1.4.4
	github.com/google/certificate-transparency-go v1.1.1
	github.com/google/trillian v1.3.13
	github.com/prometheus/client_golang v1.9.0
	golang.org/x/net v0.0.0-20200625001655-4c5254603344
	google.golang.org/grpc v1.36.0
)
